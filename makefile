COMPILER = latexmk
TO_TEXT = pdftotext

CV_TARGET = resume.pdf
TRANSCRIPT_TARGET = academicTranscript.pdf
LETTER_TARGET = cover-letter.pdf

CV_DIR = cv
TRANSCRIPT_DIR = transcript
LETTER_DIR = cover-letter

CV = $(CV_DIR)/$(CV_TARGET)
TRANSCRIPT = $(TRANSCRIPT_DIR)/$(TRANSCRIPT_TARGET)
LETTER = $(LETTER_DIR)/$(LETTER_TARGET)

CV_IN = resume.tex
TRANSCRIPT_IN = academicTranscript.tex
LETTER_IN = cover-letter.tex

all: $(CV) $(LETTER) $(TRANSCRIPT)

cv: $(CV)

resume: $(CV)

$(CV): $(CV_DIR)/$(CV_IN)
	$(COMPILER) -pdf -jobname=$(CV_DIR) $(CV_DIR)/$(CV_IN)

$(TRANSCRIPT):
	$(COMPILER) -pdf -jobname=$(TRANSCRIPT_DIR) $(TRANSCRIPT_DIR)/$(TRANSCRIPT_IN)

$(LETTER):
	$(COMPILER) -pdf -jobname=$(LETTER_DIR) $(LETTER_DIR)/$(LETTER_IN)
	

clean:
	rm -f $(LETTER_DIR)/*~ $(LETTER_DIR)/*.aux $(LETTER_DIR)/*.log $(LETTER_DIR)/*.txt $(LETTER_DIR)/*.pdf $(LETTER_DIR)/*.log
	rm -f $(TRANSCRIPT_DIR)/*~ $(TRANSCRIPT_DIR)/*.aux $(TRANSCRIPT_DIR)/*.log $(TRANSCRIPT_DIR)/*.txt $(TRANSCRIPT_DIR)/*.pdf $(TRANSCRIPT_DIR)/*.log
	rm -f $(CV_DIR)/*~ $(CV_DIR)/*.aux $(CV_DIR)/*.log $(CV_DIR)/*.txt $(CV_DIR)/*.pdf $(CV_DIR)/*.log
	rm -f $(LETTER_DIR)/.*.swp $(CV_DIR)/.*.swp $(TRANSCRIPT_DIR)/.*.swp ./.*.swp ./*~
	rm -f $(LETTER_DIR)/*.fls $(CV_DIR)/*.fls $(TRANSCRIPT_DIR)/*.fls
	rm -f $(LETTER_DIR)/*.fdb_latexmk $(CV_DIR)/*.fdb_latexmk $(TRANSCRIPT_DIR)/*.fdb_latexmk
	rm -f ./*~ ./*.aux $./*.log ./*.txt ./*.fdb_latexmk ./*.fls ./*.log
